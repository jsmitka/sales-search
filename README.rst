=============================
Sales-search
=============================

.. image:: https://badge.fury.io/py/sales_search.png
    :target: https://badge.fury.io/py/sales_search

.. image:: https://www.codeship.io/projects/5db36ff0-931d-0131-5770-56aa14aa46b3/status

.. image:: https://coveralls.io/repos/jsmitka/sales_search/badge.png?branch=master
    :target: https://coveralls.io/r/jsmitka/sales_search?branch=master

The simple search app for Django.

Documentation
-------------

The full documentation is at https://sales_search.readthedocs.org.

Quickstart
----------

Install Sales-search::

    pip install sales_search

Then use it in a project::

    import sales_search

Features
--------

* TODO
