============
Installation
============

At the command line::

    $ easy_install sales_search

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv sales_search
    $ pip install sales_search