# -*- coding: utf-8 -*-
# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext


from models import Search


def search(request,
           template="search/search.html",
           extra_context=None):

    query = request.GET.get("q", None)

    if query and query != u"":
        query = query.replace('+', ' AND ').replace(' -', ' NOT ')
        s = Search()
        hits = s.search(query)
    else:
        hits = None

    return render_to_response(template,
                              {"hits": hits,
                               "query": query},
                              context_instance=RequestContext(request))
