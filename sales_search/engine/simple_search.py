# -*- coding: utf-8 -*-

from django.utils.importlib import import_module

from sales.search.conf import settings


class SimpleSearch(object):
    """
    Trida poskytuje jednoduche hledani.
    """
    def __init__(self):
        #Vytvori slovnik, kde klic je trida, kde se bude hledat a hodnota
        # je puvodni list poli, ktera se prohledavaji.
        self.cls_fields = {}
        for s in settings.SEARCH_IN_MODELS:
            self.cls_fields[self._get_model_class(s['model'])] = s['fields']

    def _get_model_class(self, name):
        '''
        Importuje modul a vrati tridu.

        name -- napr. "sales.search.models.SearchStat"
        '''
        module_name, klass_name = name.rsplit('.', 1)

        # Nacteni modulu a ziskani reference na tridu
        m = import_module(module_name)
        klass = getattr(m, klass_name)

        return klass

    def search(self, query):
        '''
        Metoda vyhelda objekty podle zadaneho "query".

        Vraci pole objektu.
        '''
        hits = []

        #FIXME: Vhodnejsi by bylo predelat ne jden dotaz do databaze a odfiltrovat duplicity.
        # Ted se muze stat, ze jeden objekt bude mit vice hitu, protoze bude splnena podminka
        # pro vicero volani (napr. pro titulek a obsah).
        for klass, fields in self.cls_fields.items():
            q = {}
            for field in fields:
                q = {}
                q['%s__icontains' % field] = query
                hits.extend(klass.objects.filter(**q))
            #hits.append(klass.objects.filter(q))

        return hits
