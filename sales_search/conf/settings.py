# -*- coding: utf-8 -*-
from django.conf import settings


SEARCH_COLLECT_STATS = getattr(settings,
                               "SEARCH_COLLECT_STATS",
                               False)

# list slovniku v podobe:
# ({'model': 'models.Model', 'fields': ('title', 'description',...)},...)
SEARCH_IN_MODELS = getattr(settings,
                           "SEARCH_IN_MODELS",
                           None)

SEARCH_ENGINE = getattr(settings,
                        "SEARCH_ENGINE",
                        'simple_search')

SEARCH_ENGINES = getattr(settings,
                         "SEARCH_ENGINES",
                         {'simple_search': 'sales.search.engine.simple_search.SimpleSearch'})
