# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext as _
from django.utils.importlib import import_module

from conf import settings


class SearchStat(models.Model):
    search_term = models.CharField(verbose_name=_(u'Hledaný výraz'),
                                   max_length=255)
    search_count = models.PositiveIntegerField(verbose_name=_(u'Počet hledání'),
                                               default=0)

    def __unicode__(self):
        return u'%s [%d]' % (self.search_term, self.search_count)

    class Meta:
        verbose_name = _(u"Vyhledávání")
        verbose_name_plural = _(u"Vyhledávání")


class Search(object):
    """
    Objekt zastupujici vyhledavani (proxy).
    """

    def __init__(self):
        # Ziska vyhledavaci engine
        engine = settings.SEARCH_ENGINES[settings.SEARCH_ENGINE]
        # Rozdeli retezec na tridu a nazev modulu, kde je ulozena trida
        engine_module, engine_class = engine.rsplit('.', 1)

        # Nacteni modulu a ziskani reference na tridu
        s = import_module(engine_module)
        search_engine = getattr(s, engine_class)

        # Ulozeni instance tridy vyhledavani
        self.engine = search_engine()

    def search(self, query):
        '''
        Vyhledavaci metoda.
        '''
        if settings.SEARCH_COLLECT_STATS:
            s, created = SearchStat.objects.get_or_create(search_term=query,
                                                          defaults={'search_count': 0})
            s.search_count += 1
            s.save()

        return self.engine.search(query)


class SearchInterface(object):
    '''
    Predek vsech vyhledavacich modulu (enginu).
    '''

    def __init__(self):
        pass

    def search(self, query):
        pass
