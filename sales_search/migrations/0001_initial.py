# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):
    
    def forwards(self, orm):
        
        # Adding model 'SearchStat'
        db.create_table('search_searchstat', (
            ('search_term', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('search_count', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('search', ['SearchStat'])
    
    
    def backwards(self, orm):
        
        # Deleting model 'SearchStat'
        db.delete_table('search_searchstat')
    
    
    models = {
        'search.searchstat': {
            'Meta': {'object_name': 'SearchStat'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'search_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'search_term': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }
    
    complete_apps = ['search']
