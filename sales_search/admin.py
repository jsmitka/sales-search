# -*- coding: utf-8 -*-

from django.contrib import admin
from django.utils.translation import ugettext

from models import SearchStat


class SearchStatAdmin(admin.ModelAdmin):
    pass

admin.site.register(SearchStat, SearchStatAdmin)
